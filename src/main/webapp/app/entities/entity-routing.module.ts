import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'caserne',
        data: { pageTitle: 'myappApp.caserne.home.title' },
        loadChildren: () => import('./caserne/caserne.module').then(m => m.CaserneModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}

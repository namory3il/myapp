import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { ICaserne, getCaserneIdentifier } from '../caserne.model';

export type EntityResponseType = HttpResponse<ICaserne>;
export type EntityArrayResponseType = HttpResponse<ICaserne[]>;

@Injectable({ providedIn: 'root' })
export class CaserneService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/casernes');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(caserne: ICaserne): Observable<EntityResponseType> {
    return this.http.post<ICaserne>(this.resourceUrl, caserne, { observe: 'response' });
  }

  update(caserne: ICaserne): Observable<EntityResponseType> {
    return this.http.put<ICaserne>(`${this.resourceUrl}/${getCaserneIdentifier(caserne) as number}`, caserne, { observe: 'response' });
  }

  partialUpdate(caserne: ICaserne): Observable<EntityResponseType> {
    return this.http.patch<ICaserne>(`${this.resourceUrl}/${getCaserneIdentifier(caserne) as number}`, caserne, { observe: 'response' });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<ICaserne>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<ICaserne[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addCaserneToCollectionIfMissing(caserneCollection: ICaserne[], ...casernesToCheck: (ICaserne | null | undefined)[]): ICaserne[] {
    const casernes: ICaserne[] = casernesToCheck.filter(isPresent);
    if (casernes.length > 0) {
      const caserneCollectionIdentifiers = caserneCollection.map(caserneItem => getCaserneIdentifier(caserneItem)!);
      const casernesToAdd = casernes.filter(caserneItem => {
        const caserneIdentifier = getCaserneIdentifier(caserneItem);
        if (caserneIdentifier == null || caserneCollectionIdentifiers.includes(caserneIdentifier)) {
          return false;
        }
        caserneCollectionIdentifiers.push(caserneIdentifier);
        return true;
      });
      return [...casernesToAdd, ...caserneCollection];
    }
    return caserneCollection;
  }
}

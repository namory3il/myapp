import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { ICaserne, Caserne } from '../caserne.model';

import { CaserneService } from './caserne.service';

describe('Caserne Service', () => {
  let service: CaserneService;
  let httpMock: HttpTestingController;
  let elemDefault: ICaserne;
  let expectedResult: ICaserne | ICaserne[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(CaserneService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      caserne: 'AAAAAAA',
      numero: 0,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a Caserne', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new Caserne()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a Caserne', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          caserne: 'BBBBBB',
          numero: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a Caserne', () => {
      const patchObject = Object.assign({}, new Caserne());

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of Caserne', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          caserne: 'BBBBBB',
          numero: 1,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a Caserne', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addCaserneToCollectionIfMissing', () => {
      it('should add a Caserne to an empty array', () => {
        const caserne: ICaserne = { id: 123 };
        expectedResult = service.addCaserneToCollectionIfMissing([], caserne);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(caserne);
      });

      it('should not add a Caserne to an array that contains it', () => {
        const caserne: ICaserne = { id: 123 };
        const caserneCollection: ICaserne[] = [
          {
            ...caserne,
          },
          { id: 456 },
        ];
        expectedResult = service.addCaserneToCollectionIfMissing(caserneCollection, caserne);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a Caserne to an array that doesn't contain it", () => {
        const caserne: ICaserne = { id: 123 };
        const caserneCollection: ICaserne[] = [{ id: 456 }];
        expectedResult = service.addCaserneToCollectionIfMissing(caserneCollection, caserne);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(caserne);
      });

      it('should add only unique Caserne to an array', () => {
        const caserneArray: ICaserne[] = [{ id: 123 }, { id: 456 }, { id: 51149 }];
        const caserneCollection: ICaserne[] = [{ id: 123 }];
        expectedResult = service.addCaserneToCollectionIfMissing(caserneCollection, ...caserneArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const caserne: ICaserne = { id: 123 };
        const caserne2: ICaserne = { id: 456 };
        expectedResult = service.addCaserneToCollectionIfMissing([], caserne, caserne2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(caserne);
        expect(expectedResult).toContain(caserne2);
      });

      it('should accept null and undefined values', () => {
        const caserne: ICaserne = { id: 123 };
        expectedResult = service.addCaserneToCollectionIfMissing([], null, caserne, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(caserne);
      });

      it('should return initial array if no Caserne is added', () => {
        const caserneCollection: ICaserne[] = [{ id: 123 }];
        expectedResult = service.addCaserneToCollectionIfMissing(caserneCollection, undefined, null);
        expect(expectedResult).toEqual(caserneCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { ICaserne } from '../caserne.model';

@Component({
  selector: 'jhi-caserne-detail',
  templateUrl: './caserne-detail.component.html',
})
export class CaserneDetailComponent implements OnInit {
  caserne: ICaserne | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ caserne }) => {
      this.caserne = caserne;
    });
  }

  previousState(): void {
    window.history.back();
  }
}

import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { CaserneDetailComponent } from './caserne-detail.component';

describe('Caserne Management Detail Component', () => {
  let comp: CaserneDetailComponent;
  let fixture: ComponentFixture<CaserneDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [CaserneDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ caserne: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(CaserneDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(CaserneDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load caserne on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.caserne).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});

import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { ICaserne, Caserne } from '../caserne.model';
import { CaserneService } from '../service/caserne.service';

@Injectable({ providedIn: 'root' })
export class CaserneRoutingResolveService implements Resolve<ICaserne> {
  constructor(protected service: CaserneService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<ICaserne> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((caserne: HttpResponse<Caserne>) => {
          if (caserne.body) {
            return of(caserne.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new Caserne());
  }
}

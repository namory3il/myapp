import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { CaserneComponent } from '../list/caserne.component';
import { CaserneDetailComponent } from '../detail/caserne-detail.component';
import { CaserneUpdateComponent } from '../update/caserne-update.component';
import { CaserneRoutingResolveService } from './caserne-routing-resolve.service';

const caserneRoute: Routes = [
  {
    path: '',
    component: CaserneComponent,
    data: {
      defaultSort: 'id,asc',
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: CaserneDetailComponent,
    resolve: {
      caserne: CaserneRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: CaserneUpdateComponent,
    resolve: {
      caserne: CaserneRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: CaserneUpdateComponent,
    resolve: {
      caserne: CaserneRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(caserneRoute)],
  exports: [RouterModule],
})
export class CaserneRoutingModule {}

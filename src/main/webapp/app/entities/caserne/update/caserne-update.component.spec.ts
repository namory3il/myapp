import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { CaserneService } from '../service/caserne.service';
import { ICaserne, Caserne } from '../caserne.model';

import { CaserneUpdateComponent } from './caserne-update.component';

describe('Caserne Management Update Component', () => {
  let comp: CaserneUpdateComponent;
  let fixture: ComponentFixture<CaserneUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let caserneService: CaserneService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [CaserneUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(CaserneUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(CaserneUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    caserneService = TestBed.inject(CaserneService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const caserne: ICaserne = { id: 456 };

      activatedRoute.data = of({ caserne });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(caserne));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Caserne>>();
      const caserne = { id: 123 };
      jest.spyOn(caserneService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ caserne });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: caserne }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(caserneService.update).toHaveBeenCalledWith(caserne);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Caserne>>();
      const caserne = new Caserne();
      jest.spyOn(caserneService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ caserne });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: caserne }));
      saveSubject.complete();

      // THEN
      expect(caserneService.create).toHaveBeenCalledWith(caserne);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Caserne>>();
      const caserne = { id: 123 };
      jest.spyOn(caserneService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ caserne });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(caserneService.update).toHaveBeenCalledWith(caserne);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});

import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { ICaserne, Caserne } from '../caserne.model';
import { CaserneService } from '../service/caserne.service';

@Component({
  selector: 'jhi-caserne-update',
  templateUrl: './caserne-update.component.html',
})
export class CaserneUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    caserne: [],
    numero: [],
  });

  constructor(protected caserneService: CaserneService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ caserne }) => {
      this.updateForm(caserne);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const caserne = this.createFromForm();
    if (caserne.id !== undefined) {
      this.subscribeToSaveResponse(this.caserneService.update(caserne));
    } else {
      this.subscribeToSaveResponse(this.caserneService.create(caserne));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ICaserne>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(caserne: ICaserne): void {
    this.editForm.patchValue({
      id: caserne.id,
      caserne: caserne.caserne,
      numero: caserne.numero,
    });
  }

  protected createFromForm(): ICaserne {
    return {
      ...new Caserne(),
      id: this.editForm.get(['id'])!.value,
      caserne: this.editForm.get(['caserne'])!.value,
      numero: this.editForm.get(['numero'])!.value,
    };
  }
}

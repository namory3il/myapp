import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { ICaserne } from '../caserne.model';
import { CaserneService } from '../service/caserne.service';

@Component({
  templateUrl: './caserne-delete-dialog.component.html',
})
export class CaserneDeleteDialogComponent {
  caserne?: ICaserne;

  constructor(protected caserneService: CaserneService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.caserneService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}

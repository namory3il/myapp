export interface ICaserne {
  id?: number;
  caserne?: string | null;
  numero?: number | null;
}

export class Caserne implements ICaserne {
  constructor(public id?: number, public caserne?: string | null, public numero?: number | null) {}
}

export function getCaserneIdentifier(caserne: ICaserne): number | undefined {
  return caserne.id;
}

import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { CaserneComponent } from './list/caserne.component';
import { CaserneDetailComponent } from './detail/caserne-detail.component';
import { CaserneUpdateComponent } from './update/caserne-update.component';
import { CaserneDeleteDialogComponent } from './delete/caserne-delete-dialog.component';
import { CaserneRoutingModule } from './route/caserne-routing.module';

@NgModule({
  imports: [SharedModule, CaserneRoutingModule],
  declarations: [CaserneComponent, CaserneDetailComponent, CaserneUpdateComponent, CaserneDeleteDialogComponent],
  entryComponents: [CaserneDeleteDialogComponent],
})
export class CaserneModule {}

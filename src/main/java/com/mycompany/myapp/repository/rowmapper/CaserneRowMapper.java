package com.mycompany.myapp.repository.rowmapper;

import com.mycompany.myapp.domain.Caserne;
import io.r2dbc.spi.Row;
import java.util.function.BiFunction;
import org.springframework.stereotype.Service;

/**
 * Converter between {@link Row} to {@link Caserne}, with proper type conversions.
 */
@Service
public class CaserneRowMapper implements BiFunction<Row, String, Caserne> {

    private final ColumnConverter converter;

    public CaserneRowMapper(ColumnConverter converter) {
        this.converter = converter;
    }

    /**
     * Take a {@link Row} and a column prefix, and extract all the fields.
     * @return the {@link Caserne} stored in the database.
     */
    @Override
    public Caserne apply(Row row, String prefix) {
        Caserne entity = new Caserne();
        entity.setId(converter.fromRow(row, prefix + "_id", Long.class));
        entity.setCaserne(converter.fromRow(row, prefix + "_caserne", String.class));
        entity.setNumero(converter.fromRow(row, prefix + "_numero", Integer.class));
        return entity;
    }
}

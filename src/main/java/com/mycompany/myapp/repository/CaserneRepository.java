package com.mycompany.myapp.repository;

import com.mycompany.myapp.domain.Caserne;
import org.springframework.data.domain.Pageable;
import org.springframework.data.r2dbc.repository.Query;
import org.springframework.data.relational.core.query.Criteria;
import org.springframework.data.repository.reactive.ReactiveCrudRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

/**
 * Spring Data SQL reactive repository for the Caserne entity.
 */
@SuppressWarnings("unused")
@Repository
public interface CaserneRepository extends ReactiveCrudRepository<Caserne, Long>, CaserneRepositoryInternal {
    Flux<Caserne> findAllBy(Pageable pageable);

    @Override
    <S extends Caserne> Mono<S> save(S entity);

    @Override
    Flux<Caserne> findAll();

    @Override
    Mono<Caserne> findById(Long id);

    @Override
    Mono<Void> deleteById(Long id);
}

interface CaserneRepositoryInternal {
    <S extends Caserne> Mono<S> save(S entity);

    Flux<Caserne> findAllBy(Pageable pageable);

    Flux<Caserne> findAll();

    Mono<Caserne> findById(Long id);

    Flux<Caserne> findAllBy(Pageable pageable, Criteria criteria);
}

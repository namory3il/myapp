package com.mycompany.myapp.web.rest;

import com.mycompany.myapp.domain.Caserne;
import com.mycompany.myapp.repository.CaserneRepository;
import com.mycompany.myapp.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.ServerHttpRequest;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.PaginationUtil;
import tech.jhipster.web.util.reactive.ResponseUtil;

/**
 * REST controller for managing {@link com.mycompany.myapp.domain.Caserne}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class CaserneResource {

    private final Logger log = LoggerFactory.getLogger(CaserneResource.class);

    private static final String ENTITY_NAME = "caserne";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final CaserneRepository caserneRepository;

    public CaserneResource(CaserneRepository caserneRepository) {
        this.caserneRepository = caserneRepository;
    }

    /**
     * {@code POST  /casernes} : Create a new caserne.
     *
     * @param caserne the caserne to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new caserne, or with status {@code 400 (Bad Request)} if the caserne has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/casernes")
    public Mono<ResponseEntity<Caserne>> createCaserne(@RequestBody Caserne caserne) throws URISyntaxException {
        log.debug("REST request to save Caserne : {}", caserne);
        if (caserne.getId() != null) {
            throw new BadRequestAlertException("A new caserne cannot already have an ID", ENTITY_NAME, "idexists");
        }
        return caserneRepository
            .save(caserne)
            .map(result -> {
                try {
                    return ResponseEntity
                        .created(new URI("/api/casernes/" + result.getId()))
                        .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                        .body(result);
                } catch (URISyntaxException e) {
                    throw new RuntimeException(e);
                }
            });
    }

    /**
     * {@code PUT  /casernes/:id} : Updates an existing caserne.
     *
     * @param id the id of the caserne to save.
     * @param caserne the caserne to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated caserne,
     * or with status {@code 400 (Bad Request)} if the caserne is not valid,
     * or with status {@code 500 (Internal Server Error)} if the caserne couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/casernes/{id}")
    public Mono<ResponseEntity<Caserne>> updateCaserne(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Caserne caserne
    ) throws URISyntaxException {
        log.debug("REST request to update Caserne : {}, {}", id, caserne);
        if (caserne.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, caserne.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return caserneRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                return caserneRepository
                    .save(caserne)
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(result ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
                            .body(result)
                    );
            });
    }

    /**
     * {@code PATCH  /casernes/:id} : Partial updates given fields of an existing caserne, field will ignore if it is null
     *
     * @param id the id of the caserne to save.
     * @param caserne the caserne to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated caserne,
     * or with status {@code 400 (Bad Request)} if the caserne is not valid,
     * or with status {@code 404 (Not Found)} if the caserne is not found,
     * or with status {@code 500 (Internal Server Error)} if the caserne couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/casernes/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public Mono<ResponseEntity<Caserne>> partialUpdateCaserne(
        @PathVariable(value = "id", required = false) final Long id,
        @RequestBody Caserne caserne
    ) throws URISyntaxException {
        log.debug("REST request to partial update Caserne partially : {}, {}", id, caserne);
        if (caserne.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, caserne.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        return caserneRepository
            .existsById(id)
            .flatMap(exists -> {
                if (!exists) {
                    return Mono.error(new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound"));
                }

                Mono<Caserne> result = caserneRepository
                    .findById(caserne.getId())
                    .map(existingCaserne -> {
                        if (caserne.getCaserne() != null) {
                            existingCaserne.setCaserne(caserne.getCaserne());
                        }
                        if (caserne.getNumero() != null) {
                            existingCaserne.setNumero(caserne.getNumero());
                        }

                        return existingCaserne;
                    })
                    .flatMap(caserneRepository::save);

                return result
                    .switchIfEmpty(Mono.error(new ResponseStatusException(HttpStatus.NOT_FOUND)))
                    .map(res ->
                        ResponseEntity
                            .ok()
                            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, res.getId().toString()))
                            .body(res)
                    );
            });
    }

    /**
     * {@code GET  /casernes} : get all the casernes.
     *
     * @param pageable the pagination information.
     * @param request a {@link ServerHttpRequest} request.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of casernes in body.
     */
    @GetMapping("/casernes")
    public Mono<ResponseEntity<List<Caserne>>> getAllCasernes(
        @org.springdoc.api.annotations.ParameterObject Pageable pageable,
        ServerHttpRequest request
    ) {
        log.debug("REST request to get a page of Casernes");
        return caserneRepository
            .count()
            .zipWith(caserneRepository.findAllBy(pageable).collectList())
            .map(countWithEntities ->
                ResponseEntity
                    .ok()
                    .headers(
                        PaginationUtil.generatePaginationHttpHeaders(
                            UriComponentsBuilder.fromHttpRequest(request),
                            new PageImpl<>(countWithEntities.getT2(), pageable, countWithEntities.getT1())
                        )
                    )
                    .body(countWithEntities.getT2())
            );
    }

    /**
     * {@code GET  /casernes/:id} : get the "id" caserne.
     *
     * @param id the id of the caserne to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the caserne, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/casernes/{id}")
    public Mono<ResponseEntity<Caserne>> getCaserne(@PathVariable Long id) {
        log.debug("REST request to get Caserne : {}", id);
        Mono<Caserne> caserne = caserneRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(caserne);
    }

    /**
     * {@code DELETE  /casernes/:id} : delete the "id" caserne.
     *
     * @param id the id of the caserne to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/casernes/{id}")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public Mono<ResponseEntity<Void>> deleteCaserne(@PathVariable Long id) {
        log.debug("REST request to delete Caserne : {}", id);
        return caserneRepository
            .deleteById(id)
            .map(result ->
                ResponseEntity
                    .noContent()
                    .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
                    .build()
            );
    }
}

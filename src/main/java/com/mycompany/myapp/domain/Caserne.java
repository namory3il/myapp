package com.mycompany.myapp.domain;

import java.io.Serializable;
import org.springframework.data.annotation.Id;
import org.springframework.data.annotation.Transient;
import org.springframework.data.relational.core.mapping.Column;
import org.springframework.data.relational.core.mapping.Table;

/**
 * A Caserne.
 */
@Table("caserne")
public class Caserne implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @Column("id")
    private Long id;

    @Column("caserne")
    private String caserne;

    @Column("numero")
    private Integer numero;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Caserne id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCaserne() {
        return this.caserne;
    }

    public Caserne caserne(String caserne) {
        this.setCaserne(caserne);
        return this;
    }

    public void setCaserne(String caserne) {
        this.caserne = caserne;
    }

    public Integer getNumero() {
        return this.numero;
    }

    public Caserne numero(Integer numero) {
        this.setNumero(numero);
        return this;
    }

    public void setNumero(Integer numero) {
        this.numero = numero;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Caserne)) {
            return false;
        }
        return id != null && id.equals(((Caserne) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Caserne{" +
            "id=" + getId() +
            ", caserne='" + getCaserne() + "'" +
            ", numero=" + getNumero() +
            "}";
    }
}

package com.mycompany.myapp.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.is;

import com.mycompany.myapp.IntegrationTest;
import com.mycompany.myapp.domain.Caserne;
import com.mycompany.myapp.repository.CaserneRepository;
import com.mycompany.myapp.repository.EntityManager;
import java.time.Duration;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.reactive.server.WebTestClient;

/**
 * Integration tests for the {@link CaserneResource} REST controller.
 */
@IntegrationTest
@AutoConfigureWebTestClient(timeout = IntegrationTest.DEFAULT_ENTITY_TIMEOUT)
@WithMockUser
class CaserneResourceIT {

    private static final String DEFAULT_CASERNE = "AAAAAAAAAA";
    private static final String UPDATED_CASERNE = "BBBBBBBBBB";

    private static final Integer DEFAULT_NUMERO = 1;
    private static final Integer UPDATED_NUMERO = 2;

    private static final String ENTITY_API_URL = "/api/casernes";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private CaserneRepository caserneRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private WebTestClient webTestClient;

    private Caserne caserne;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Caserne createEntity(EntityManager em) {
        Caserne caserne = new Caserne().caserne(DEFAULT_CASERNE).numero(DEFAULT_NUMERO);
        return caserne;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Caserne createUpdatedEntity(EntityManager em) {
        Caserne caserne = new Caserne().caserne(UPDATED_CASERNE).numero(UPDATED_NUMERO);
        return caserne;
    }

    public static void deleteEntities(EntityManager em) {
        try {
            em.deleteAll(Caserne.class).block();
        } catch (Exception e) {
            // It can fail, if other entities are still referring this - it will be removed later.
        }
    }

    @AfterEach
    public void cleanup() {
        deleteEntities(em);
    }

    @BeforeEach
    public void initTest() {
        deleteEntities(em);
        caserne = createEntity(em);
    }

    @Test
    void createCaserne() throws Exception {
        int databaseSizeBeforeCreate = caserneRepository.findAll().collectList().block().size();
        // Create the Caserne
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(caserne))
            .exchange()
            .expectStatus()
            .isCreated();

        // Validate the Caserne in the database
        List<Caserne> caserneList = caserneRepository.findAll().collectList().block();
        assertThat(caserneList).hasSize(databaseSizeBeforeCreate + 1);
        Caserne testCaserne = caserneList.get(caserneList.size() - 1);
        assertThat(testCaserne.getCaserne()).isEqualTo(DEFAULT_CASERNE);
        assertThat(testCaserne.getNumero()).isEqualTo(DEFAULT_NUMERO);
    }

    @Test
    void createCaserneWithExistingId() throws Exception {
        // Create the Caserne with an existing ID
        caserne.setId(1L);

        int databaseSizeBeforeCreate = caserneRepository.findAll().collectList().block().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        webTestClient
            .post()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(caserne))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Caserne in the database
        List<Caserne> caserneList = caserneRepository.findAll().collectList().block();
        assertThat(caserneList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    void getAllCasernes() {
        // Initialize the database
        caserneRepository.save(caserne).block();

        // Get all the caserneList
        webTestClient
            .get()
            .uri(ENTITY_API_URL + "?sort=id,desc")
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.[*].id")
            .value(hasItem(caserne.getId().intValue()))
            .jsonPath("$.[*].caserne")
            .value(hasItem(DEFAULT_CASERNE))
            .jsonPath("$.[*].numero")
            .value(hasItem(DEFAULT_NUMERO));
    }

    @Test
    void getCaserne() {
        // Initialize the database
        caserneRepository.save(caserne).block();

        // Get the caserne
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, caserne.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isOk()
            .expectHeader()
            .contentType(MediaType.APPLICATION_JSON)
            .expectBody()
            .jsonPath("$.id")
            .value(is(caserne.getId().intValue()))
            .jsonPath("$.caserne")
            .value(is(DEFAULT_CASERNE))
            .jsonPath("$.numero")
            .value(is(DEFAULT_NUMERO));
    }

    @Test
    void getNonExistingCaserne() {
        // Get the caserne
        webTestClient
            .get()
            .uri(ENTITY_API_URL_ID, Long.MAX_VALUE)
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNotFound();
    }

    @Test
    void putNewCaserne() throws Exception {
        // Initialize the database
        caserneRepository.save(caserne).block();

        int databaseSizeBeforeUpdate = caserneRepository.findAll().collectList().block().size();

        // Update the caserne
        Caserne updatedCaserne = caserneRepository.findById(caserne.getId()).block();
        updatedCaserne.caserne(UPDATED_CASERNE).numero(UPDATED_NUMERO);

        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, updatedCaserne.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(updatedCaserne))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Caserne in the database
        List<Caserne> caserneList = caserneRepository.findAll().collectList().block();
        assertThat(caserneList).hasSize(databaseSizeBeforeUpdate);
        Caserne testCaserne = caserneList.get(caserneList.size() - 1);
        assertThat(testCaserne.getCaserne()).isEqualTo(UPDATED_CASERNE);
        assertThat(testCaserne.getNumero()).isEqualTo(UPDATED_NUMERO);
    }

    @Test
    void putNonExistingCaserne() throws Exception {
        int databaseSizeBeforeUpdate = caserneRepository.findAll().collectList().block().size();
        caserne.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, caserne.getId())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(caserne))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Caserne in the database
        List<Caserne> caserneList = caserneRepository.findAll().collectList().block();
        assertThat(caserneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithIdMismatchCaserne() throws Exception {
        int databaseSizeBeforeUpdate = caserneRepository.findAll().collectList().block().size();
        caserne.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(caserne))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Caserne in the database
        List<Caserne> caserneList = caserneRepository.findAll().collectList().block();
        assertThat(caserneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void putWithMissingIdPathParamCaserne() throws Exception {
        int databaseSizeBeforeUpdate = caserneRepository.findAll().collectList().block().size();
        caserne.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .put()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.APPLICATION_JSON)
            .bodyValue(TestUtil.convertObjectToJsonBytes(caserne))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Caserne in the database
        List<Caserne> caserneList = caserneRepository.findAll().collectList().block();
        assertThat(caserneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void partialUpdateCaserneWithPatch() throws Exception {
        // Initialize the database
        caserneRepository.save(caserne).block();

        int databaseSizeBeforeUpdate = caserneRepository.findAll().collectList().block().size();

        // Update the caserne using partial update
        Caserne partialUpdatedCaserne = new Caserne();
        partialUpdatedCaserne.setId(caserne.getId());

        partialUpdatedCaserne.numero(UPDATED_NUMERO);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedCaserne.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedCaserne))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Caserne in the database
        List<Caserne> caserneList = caserneRepository.findAll().collectList().block();
        assertThat(caserneList).hasSize(databaseSizeBeforeUpdate);
        Caserne testCaserne = caserneList.get(caserneList.size() - 1);
        assertThat(testCaserne.getCaserne()).isEqualTo(DEFAULT_CASERNE);
        assertThat(testCaserne.getNumero()).isEqualTo(UPDATED_NUMERO);
    }

    @Test
    void fullUpdateCaserneWithPatch() throws Exception {
        // Initialize the database
        caserneRepository.save(caserne).block();

        int databaseSizeBeforeUpdate = caserneRepository.findAll().collectList().block().size();

        // Update the caserne using partial update
        Caserne partialUpdatedCaserne = new Caserne();
        partialUpdatedCaserne.setId(caserne.getId());

        partialUpdatedCaserne.caserne(UPDATED_CASERNE).numero(UPDATED_NUMERO);

        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, partialUpdatedCaserne.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(partialUpdatedCaserne))
            .exchange()
            .expectStatus()
            .isOk();

        // Validate the Caserne in the database
        List<Caserne> caserneList = caserneRepository.findAll().collectList().block();
        assertThat(caserneList).hasSize(databaseSizeBeforeUpdate);
        Caserne testCaserne = caserneList.get(caserneList.size() - 1);
        assertThat(testCaserne.getCaserne()).isEqualTo(UPDATED_CASERNE);
        assertThat(testCaserne.getNumero()).isEqualTo(UPDATED_NUMERO);
    }

    @Test
    void patchNonExistingCaserne() throws Exception {
        int databaseSizeBeforeUpdate = caserneRepository.findAll().collectList().block().size();
        caserne.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, caserne.getId())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(caserne))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Caserne in the database
        List<Caserne> caserneList = caserneRepository.findAll().collectList().block();
        assertThat(caserneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithIdMismatchCaserne() throws Exception {
        int databaseSizeBeforeUpdate = caserneRepository.findAll().collectList().block().size();
        caserne.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL_ID, count.incrementAndGet())
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(caserne))
            .exchange()
            .expectStatus()
            .isBadRequest();

        // Validate the Caserne in the database
        List<Caserne> caserneList = caserneRepository.findAll().collectList().block();
        assertThat(caserneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void patchWithMissingIdPathParamCaserne() throws Exception {
        int databaseSizeBeforeUpdate = caserneRepository.findAll().collectList().block().size();
        caserne.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        webTestClient
            .patch()
            .uri(ENTITY_API_URL)
            .contentType(MediaType.valueOf("application/merge-patch+json"))
            .bodyValue(TestUtil.convertObjectToJsonBytes(caserne))
            .exchange()
            .expectStatus()
            .isEqualTo(405);

        // Validate the Caserne in the database
        List<Caserne> caserneList = caserneRepository.findAll().collectList().block();
        assertThat(caserneList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    void deleteCaserne() {
        // Initialize the database
        caserneRepository.save(caserne).block();

        int databaseSizeBeforeDelete = caserneRepository.findAll().collectList().block().size();

        // Delete the caserne
        webTestClient
            .delete()
            .uri(ENTITY_API_URL_ID, caserne.getId())
            .accept(MediaType.APPLICATION_JSON)
            .exchange()
            .expectStatus()
            .isNoContent();

        // Validate the database contains one less item
        List<Caserne> caserneList = caserneRepository.findAll().collectList().block();
        assertThat(caserneList).hasSize(databaseSizeBeforeDelete - 1);
    }
}

package com.mycompany.myapp.domain;

import static org.assertj.core.api.Assertions.assertThat;

import com.mycompany.myapp.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class CaserneTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(Caserne.class);
        Caserne caserne1 = new Caserne();
        caserne1.setId(1L);
        Caserne caserne2 = new Caserne();
        caserne2.setId(caserne1.getId());
        assertThat(caserne1).isEqualTo(caserne2);
        caserne2.setId(2L);
        assertThat(caserne1).isNotEqualTo(caserne2);
        caserne1.setId(null);
        assertThat(caserne1).isNotEqualTo(caserne2);
    }
}
